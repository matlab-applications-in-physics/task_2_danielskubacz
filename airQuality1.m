%MATLAB 2020b
%Name: airQuality
%Author: Daniel Skubacz
%Date: 25.11.2020

%replacing commas by points and saving it into a new file
Data2 = fileread('dane-pomiarowe_2020-11-02.csv');
Data2 = strrep(Data2, ',', '.');
New_Data2 = fopen('poprawione_dane-pomiarowe_2020-11-02.csv', 'w');
fwrite(New_Data2, Data2, 'char');
fclose(New_Data2);

%same operation for every csv data:
Data3 = fileread('dane-pomiarowe_2020-11-03.csv');
Data3 = strrep(Data3, ',', '.');
New_Data3 = fopen('poprawione_dane-pomiarowe_2020-11-03.csv', 'w');
fwrite(New_Data3, Data3, 'char');
fclose(New_Data3);

Data4 = fileread('dane-pomiarowe_2020-11-04.csv');
Data4 = strrep(Data4, ',', '.');
New_Data4 = fopen('poprawione_dane-pomiarowe_2020-11-04.csv', 'w');
fwrite(New_Data4, Data4, 'char');
fclose(New_Data4);

Data5 = fileread('dane-pomiarowe_2020-11-05.csv');
Data5 = strrep(Data5, ',', '.');
New_Data5 = fopen('poprawione_dane-pomiarowe_2020-11-05.csv', 'w');
fwrite(New_Data5, Data5, 'char');
fclose(New_Data5);

Data6 = fileread('dane-pomiarowe_2020-11-06.csv');
Data6 = strrep(Data6, ',', '.');
New_Data6 = fopen('poprawione_dane-pomiarowe_2020-11-06.csv', 'w');
fwrite(New_Data6, Data6, 'char');
fclose(New_Data6);

Data7 = fileread('dane-pomiarowe_2020-11-07.csv');
Data7 = strrep(Data7, ',', '.');
New_Data7 = fopen('poprawione_dane-pomiarowe_2020-11-07.csv', 'w');
fwrite(New_Data7, Data7, 'char');
fclose(New_Data7);

Data8 = fileread('dane-pomiarowe_2020-11-08.csv');
Data8 = strrep(Data8, ',', '.');
New_Data8 = fopen('poprawione_dane-pomiarowe_2020-11-08.csv', 'w');
fwrite(New_Data8, Data8, 'char');
fclose(New_Data8);

Data9 = fileread('dane-pomiarowe_2020-11-09.csv');
Data9 = strrep(Data9, ',', '.');
New_Data9 = fopen('poprawione_dane-pomiarowe_2020-11-09.csv', 'w');
fwrite(New_Data9, Data9, 'char');
fclose(New_Data9);

Data10 = fileread('dane-pomiarowe_2020-11-10.csv');
Data10 = strrep(Data10, ',', '.');
New_Data10 = fopen('poprawione_dane-pomiarowe_2020-11-10.csv', 'w');
fwrite(New_Data10, Data10, 'char');
fclose(New_Data10);

Data11 = fileread('dane-pomiarowe_2020-11-11.csv');
Data11 = strrep(Data11, ',', '.');
New_Data11 = fopen('poprawione_dane-pomiarowe_2020-11-11.csv', 'w');
fwrite(New_Data11, Data11, 'char');
fclose(New_Data11);

Data12 = fileread('dane-pomiarowe_2020-11-12.csv');
Data12 = strrep(Data12, ',', '.');
New_Data12 = fopen('poprawione_dane-pomiarowe_2020-11-12.csv', 'w');
fwrite(New_Data12, Data12, 'char');
fclose(New_Data12);

Data13 = fileread('dane-pomiarowe_2020-11-13.csv');
Data13 = strrep(Data13, ',', '.');
New_Data13 = fopen('poprawione_dane-pomiarowe_2020-11-13.csv', 'w');
fwrite(New_Data13, Data13, 'char');
fclose(New_Data13);

Data14 = fileread('dane-pomiarowe_2020-11-14.csv');
Data14 = strrep(Data14, ',', '.');
New_Data14 = fopen('poprawione_dane-pomiarowe_2020-11-14.csv', 'w');
fwrite(New_Data14, Data14, 'char');
fclose(New_Data14);

Data15 = fileread('dane-pomiarowe_2020-11-15.csv');
Data15 = strrep(Data15, ',', '.');
New_Data15 = fopen('poprawione_dane-pomiarowe_2020-11-15.csv', 'w');
fwrite(New_Data15, Data15, 'char');
fclose(New_Data15);

%putting csv datas into a tables:
Table_with_02 = readtable('poprawione_dane-pomiarowe_2020-11-02.csv')
Table_with_03 = readtable('poprawione_dane-pomiarowe_2020-11-03.csv')
Table_with_04 = readtable('poprawione_dane-pomiarowe_2020-11-04.csv')
Table_with_05 = readtable('poprawione_dane-pomiarowe_2020-11-05.csv')
Table_with_06 = readtable('poprawione_dane-pomiarowe_2020-11-06.csv')
Table_with_07 = readtable('poprawione_dane-pomiarowe_2020-11-07.csv')
Table_with_08 = readtable('poprawione_dane-pomiarowe_2020-11-08.csv')
Table_with_09 = readtable('poprawione_dane-pomiarowe_2020-11-09.csv')
Table_with_10 = readtable('poprawione_dane-pomiarowe_2020-11-10.csv')
Table_with_11 = readtable('poprawione_dane-pomiarowe_2020-11-11.csv')
Table_with_12 = readtable('poprawione_dane-pomiarowe_2020-11-12.csv')
Table_with_13 = readtable('poprawione_dane-pomiarowe_2020-11-13.csv')
Table_with_14 = readtable('poprawione_dane-pomiarowe_2020-11-14.csv')
Table_with_15 = readtable('poprawione_dane-pomiarowe_2020-11-15.csv')

%creating matrixes for time for every Table_with_x:
Time_2 = Table_with_02{1:24,1}
Time_3 = Table_with_03{1:24,1}
Time_4 = Table_with_04{1:24,1}
Time_5 = Table_with_05{1:24,1}
Time_6 = Table_with_06{1:24,1}
Time_7 = Table_with_07{1:24,1}
Time_8 = Table_with_08{1:24,1}
Time_9 = Table_with_09{1:24,1}
Time_10 = Table_with_10{1:24,1}
Time_11 = Table_with_11{1:24,1}
Time_12 = Table_with_12{1:24,1}
Time_13 = Table_with_13{1:24,1}
Time_14 = Table_with_14{1:24,1}
Time_15 = Table_with_15{1:24,1}

%creating matrixes for SO2, NO2, O3 and CO for every Table_with_x:
SO2_2 = Table_with_02{1:24,2}
NO2_2 = Table_with_02{1:24,3}
O3_2 = Table_with_02{1:24,7}
CO_2 = Table_with_02{1:24,9}

SO2_3 = Table_with_03{1:24,2}
NO2_3 = Table_with_03{1:24,3}
O3_3 = Table_with_03{1:24,7}
CO_3 = Table_with_03{1:24,9}

SO2_4 = Table_with_04{1:24,2}
NO2_4 = Table_with_04{1:24,3}
O3_4 = Table_with_04{1:24,7}
CO_4 = Table_with_04{1:24,9}

SO2_5 = Table_with_05{1:24,2}
NO2_5 = Table_with_05{1:24,3}
O3_5 = Table_with_05{1:24,7}
CO_5 = Table_with_05{1:24,9}

SO2_6 = Table_with_06{1:24,2}
NO2_6 = Table_with_06{1:24,3}
O3_6 = Table_with_06{1:24,7}
CO_6 = Table_with_06{1:24,9}

SO2_7 = Table_with_07{1:24,2}
NO2_7 = Table_with_07{1:24,3}
O3_7 = Table_with_07{1:24,7}
CO_7 = Table_with_07{1:24,9}

SO2_8 = Table_with_08{1:24,2}
NO2_8 = Table_with_08{1:24,3}
O3_8 = Table_with_08{1:24,7}
CO_8 = Table_with_08{1:24,9}

SO2_9 = Table_with_09{1:24,2}
NO2_9 = Table_with_09{1:24,3}
O3_9 = Table_with_09{1:24,7}
CO_9 = Table_with_09{1:24,9}

SO2_10 = Table_with_10{1:24,2}
NO2_10 = Table_with_10{1:24,3}
O3_10 = Table_with_10{1:24,7}
CO_10 = Table_with_10{1:24,9}

SO2_11 = Table_with_11{1:24,2}
NO2_11 = Table_with_11{1:24,3}
O3_11 = Table_with_11{1:24,7}
CO_11 = Table_with_11{1:24,9}

SO2_12 = Table_with_12{1:24,2}
NO2_12 = Table_with_12{1:24,3}
O3_12 = Table_with_12{1:24,7}
CO_12 = Table_with_12{1:24,9}

SO2_13 = Table_with_13{1:24,2}
NO2_13 = Table_with_13{1:24,3}
O3_13 = Table_with_13{1:24,7}
CO_13 = Table_with_13{1:24,9}

SO2_14 = Table_with_14{1:24,2}
NO2_14 = Table_with_14{1:24,3}
O3_14 = Table_with_14{1:24,7}
CO_14 = Table_with_14{1:24,9}

SO2_15 = Table_with_15{1:24,2}
NO2_15 = Table_with_15{1:24,3}
O3_15 = Table_with_15{1:24,7}
CO_15 = Table_with_15{1:24,9}

%defining maximum acceptable values for every substance:
max_SO2 = 350 %[μg/m3]
max_NO2 = 200 %[μg/m3]
max_O3 = 120 %[μg/m3]
max_CO = 10000 %[μg/m3]

%creating a file for results:
final_results = fopen('PMresults.txt', 'w');

i=1
for i=1:24
    if SO2_2(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:02.11.20', Time_2(i)])
        fwrite(final_results, SO2_2(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:02.11.20 ', Time_2(i)])
    end
end

i=1
for i=1:24
    if SO2_3(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:03.11.20', Time_3(i)])
        fwrite(final_results, SO2_3(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:03.11.20 ', Time_3(i)])
    end
end

i=1
for i=1:24
    if SO2_4(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:04.11.20', Time_4(i)])
        fwrite(final_results, SO2_4(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:04.11.20 ', Time_4(i)])
    end
end

i=1
for i=1:24
    if SO2_5(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:05.11.20', Time_5(i)])
        fwrite(final_results, SO2_5(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:05.11.20 ', Time_5(i)])
    end
end

i=1
for i=1:24
    if SO2_6(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:06.11.20', Time_6(i)])
        fwrite(final_results, SO2_6(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:06.11.20 ', Time_6(i)])
    end
end

i=1
for i=1:24
    if SO2_7(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:07.11.20', Time_7(i)])
        fwrite(final_results, SO2_7(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:07.11.20 ', Time_7(i)])
    end
end

i=1
for i=1:24
    if SO2_8(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:08.11.20', Time_8(i)])
        fwrite(final_results, SO2_8(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:08.11.20 ', Time_8(i)])
    end
end

i=1
for i=1:24
    if SO2_9(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:09.11.20', Time_9(i)])
        fwrite(final_results, SO2_9(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:09.11.20 ', Time_9(i)])
    end
end

i=1
for i=1:24
    if SO2_10(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:10.11.20', Time_10(i)])
        fwrite(final_results, SO2_10(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:10.11.20 ', Time_10(i)])
    end
end

i=1
for i=1:24
    if SO2_11(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:11.11.20', Time_11(i)])
        fwrite(final_results, SO2_11(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:11.11.20 ', Time_11(i)])
    end
end

i=1
for i=1:24
    if SO2_12(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:12.11.20', Time_12(i)])
        fwrite(final_results, SO2_12(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:12.11.20 ', Time_12(i)])
    end
end

i=1
for i=1:24
    if SO2_13(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:13.11.20', Time_13(i)])
        fwrite(final_results, SO2_13(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:13.11.20 ', Time_13(i)])
    end
end

i=1
for i=1:24
    if SO2_14(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:14.11.20', Time_14(i)])
        fwrite(final_results, SO2_14(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:14.11.20 ', Time_14(i)])
    end
end

i=1
for i=1:24
    if SO2_15(i)>max_SO2
        disp(['The value of SO2 is over the acceptable maximum! Date:15.11.20', Time_15(i)])
        fwrite(final_results, SO2_15(i), 'float');
    else
        disp(['The value of SO2 is acceptable. Date:15.11.20 ', Time_15(i)])
    end
end

